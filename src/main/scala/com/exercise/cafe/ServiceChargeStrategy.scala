package com.exercise.cafe

trait ServiceChargeStrategy {

  def calculate(l: List[Item], basePrice: BigDecimal): BigDecimal

}

class ServiceChargeImpl extends ServiceChargeStrategy {

  override def calculate(l: List[Item], basePrice: BigDecimal): BigDecimal = {
    val containsHotFood: Boolean = l.exists(i => i.itemTypes.contains(Food) && i.itemTypes.contains(Hot))

    if (containsHotFood) {
      calculateHotFood(basePrice)
    } else {
      calculateFood(l, basePrice)
    }
  }

  def calculateFood(l: List[Item], basePrice: BigDecimal): BigDecimal = {
    val containsFood: Boolean = l.exists(i => i.itemTypes.contains(Food))

    if (containsFood) {
      basePrice.*(10)./(100)
    } else {
      0
    }
  }

  def calculateHotFood(basePrice: BigDecimal): BigDecimal = {
    val charge = basePrice.*(20)./(100)
    val maxCharge = BigDecimal(20)
    if (charge > maxCharge) {
      maxCharge
    } else {
      charge
    }
  }
}
