package com.exercise.cafe

import scala.math.BigDecimal.RoundingMode

class CafeShop(menu: Map[String, Item], serviceChargeStrategy: ServiceChargeStrategy) {

  private[cafe] def convertToInternal(l: List[String]): List[Item] = {
    l.map(i => {
      menu.get(i) match {
        case Some(item) => item
        case None => throw new IllegalArgumentException("No such item on today's menu")
      }
    })
  }

  private[cafe] def calculateBasePrice(l: List[Item]): BigDecimal = l.foldLeft(BigDecimal(0))((acc, i) => acc.+(i.price))

  def calculateTotal(l: List[String]): BigDecimal = {
    val items = convertToInternal(l)
    val basePrice = calculateBasePrice(items)
    val serviceCharge = serviceChargeStrategy.calculate(items, basePrice)

    (basePrice + serviceCharge).setScale(2, RoundingMode.HALF_UP)
  }

}
