package com.exercise.cafe

import org.scalatest.{Matchers, WordSpec}

class ServiceChargeImplSpec
  extends WordSpec
  with Matchers {

  val serviceChargeImpl = new ServiceChargeImpl

  "ServiceChargeImpl" when {

    "calculate based on hot food" should {

      "yield twenty percent from base price but with a maximum of twenty pounds" in {

        val items = List(
          Item("Cola", List(Beverage), BigDecimal(0.5)),
          Item("Coffee", List(Beverage, Hot), BigDecimal(1)),
          Item("Steak Sandwich", List(Food, Hot), BigDecimal(4.5))
        )

        serviceChargeImpl.calculate(items, BigDecimal(10)) should be(BigDecimal(2))

        serviceChargeImpl.calculate(items, BigDecimal(150)) should be(BigDecimal(20))

      }
    }

    "calculate based on food" should {

      "yield ten percent from base price" in {

        val items = List(
          Item("Cola", List(Beverage), BigDecimal(0.5)),
          Item("Coffee", List(Beverage, Hot), BigDecimal(1)),
          Item("Cheese Sandwich", List(Food), BigDecimal(2))
        )

        serviceChargeImpl.calculate(items, BigDecimal(10)) should be(BigDecimal(1))

        serviceChargeImpl.calculate(items, BigDecimal(1500)) should be(BigDecimal(150))

      }
    }

    "calculate for only beverages" should {

      "yield zero" in {

        val items = List(
          Item("Cola", List(Beverage), BigDecimal(0.5)),
          Item("Coffee", List(Beverage, Hot), BigDecimal(1))
        )

        val Zero = BigDecimal(0)

        serviceChargeImpl.calculate(items, BigDecimal(10)) should be(Zero)

        serviceChargeImpl.calculate(items, BigDecimal(1500)) should be(Zero)

      }
    }
  }
}
