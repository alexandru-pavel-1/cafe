package com.exercise.cafe

import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{Matchers, WordSpec}

class CafeShopSpec
  extends WordSpec
  with Matchers
  with MockitoSugar {

  val menu = Map(
    "Cola" -> Item("Cola", List(Beverage), BigDecimal(0.5)),
    "Coffee" -> Item("Coffee", List(Beverage, Hot), BigDecimal(1)),
    "Cheese Sandwich" -> Item("Cheese Sandwich", List(Food), BigDecimal(2)),
    "Steak Sandwich" -> Item("Steak Sandwich", List(Food, Hot), BigDecimal(4.5))
  )

  val serviceChargeMock = mock[ServiceChargeStrategy]
  val cafeShop = new CafeShop(menu, serviceChargeMock)

  "CafeShop" when {

    "order existing items" should {

      val itemsForThreeAndAHalf = List(
        Item("Cola", List(Beverage), BigDecimal(0.5)),
        Item("Coffee", List(Beverage, Hot), BigDecimal(1)),
        Item("Cheese Sandwich", List(Food), BigDecimal(2))
      )

      val itemsForOneAndAHalf = List(
        Item("Cola", List(Beverage), BigDecimal(0.5)),
        Item("Coffee", List(Beverage, Hot), BigDecimal(1))
      )

      val itemsForZero = List()

      "calculate price" in {

        val threeAndAHalfPrice = cafeShop.calculateBasePrice(itemsForThreeAndAHalf)
        threeAndAHalfPrice should be(BigDecimal(3.5))

        val oneAndAHalfPrice = cafeShop.calculateBasePrice(itemsForOneAndAHalf)
        oneAndAHalfPrice should be(BigDecimal(1.5))

        val zeroPrice = cafeShop.calculateBasePrice(itemsForZero)
        zeroPrice should be(BigDecimal(0))
      }
    }

    "convert to internal" should {

      "convert to a list of items" in {
        val converted = List(
          Item("Cola", List(Beverage), BigDecimal(0.5)),
          Item("Coffee", List(Beverage, Hot), BigDecimal(1)),
          Item("Cheese Sandwich", List(Food), BigDecimal(2))
        )

        cafeShop.convertToInternal(List("Cola", "Coffee", "Cheese Sandwich")) should be(converted)
      }

      "throw IllegalArgumentException" in {

        val ex = intercept[IllegalArgumentException] {
          cafeShop.convertToInternal(List("Pepsi", "Coffee", "Cheese Sandwich"))
        }

        ex.getMessage should be("No such item on today's menu")
      }
    }

    "calculate total" should {

      "sum base price with service charge" in {
        val items = List(
          Item("Cola", List(Beverage), BigDecimal(0.5)),
          Item("Coffee", List(Beverage, Hot), BigDecimal(1)),
          Item("Cheese Sandwich", List(Food), BigDecimal(2))
        )
        val basePrice = BigDecimal(3.5)
        when(serviceChargeMock.calculate(items, basePrice)).thenReturn(BigDecimal(10.335))

        cafeShop.calculateTotal(List("Cola", "Coffee", "Cheese Sandwich")) should be(BigDecimal(13.84))

        when(serviceChargeMock.calculate(items, basePrice)).thenReturn(BigDecimal(10.334))
        cafeShop.calculateTotal(List("Cola", "Coffee", "Cheese Sandwich")) should be(BigDecimal(13.83))
      }
    }
  }

}
