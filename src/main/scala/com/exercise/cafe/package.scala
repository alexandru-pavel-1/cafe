package com.exercise

package object cafe {

  sealed trait ItemType

  object Beverage extends ItemType

  object Food extends ItemType

  object Hot extends ItemType

  case class Item(name: String, itemTypes: List[ItemType], price: BigDecimal)

}
